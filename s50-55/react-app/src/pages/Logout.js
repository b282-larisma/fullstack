// import {Navigate} from 'react-router-dom';

// export default function Logout() {

// 	localStorage.clear()
	
// 	return (
// 		<Navigate to="/login" />
// 	)
// }

import {useContext, useEffect} from 'react';

import UserContext from '../UserContext';

import {Navigate} from 'react-router-dom';

export default function Logout() {

	const {unsetUser, setUser} = useContext(UserContext);

	// localStorage.clear();
	unsetUser();

	useEffect(() => {
		setUser({id: null})
	});

	return (
		<Navigate to="/login" />
	)
}